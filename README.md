# Kumori Manifest Diff

GO package to compare kumori manifests.

## Description

This module compares two JSON documents provided in [gabs](https://github.com/Jeffail/gabs) format. Returns three slices:

* Changed: leafs found in both documents but with different values.
* Added: leafs found in the second document but not in the first one.
* Removed: leafs found in the first document but not in the second one.

Each element in those slices is a slice of strings defining the route to that leaf in the document.

## Table of contents

* [Usage](#usage)
* [License](#license)

## Usage

```go
package main

import (
  "strings"

  "github.com/Jeffail/gabs/v2"
  diff "gitlab.com/kumori-systems/community/libraries/manifest-diff"
)

func main() {
  json1, err := gabs.ParseJSON([]byte(`{
    "outter":{
      "same":{
        "value1":1,
        "value2":"string",
        "value3":true
      },
      "changed":{
        "value1":1,
        "value2":"string",
        "value3":true
      },
      "missing1":{
        "value":1
      },
      "array":{
        "value1":20,
        "array1":[
          30, 40
        ]
      }
    }
  }`))
  if err != nil {
    panic(err)
  }
  json2, err := gabs.ParseJSON([]byte(`{
    "outter":{
      "same":{
        "value1":1,
        "value2":"string",
        "value3":true
      },
      "changed":{
        "value1":2,
        "value2":"changed",
        "value3":false
      },
      "missing2":{
        "value":1
      },
      "array":{
        "value1":20,
        "array1":[
          30, 40
        ]
      }
    }
  }`))
  if err != nil {
    panic(err)
  }

  changed, added, removed := diff.Diff(json1, json2)

  for i, elem := range changed {
    fmt.Println("Changed:", elem)
  }

  for i, elem := range added {
    fmt.Println("Added:", elem)
  }

  for i, elem := range removed {
    fmt.Println("Removed:", elem)
  }

}
```

## License

Copyright 2022 Kumori systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.
