module gitlab.com/kumori-systems/community/libraries/manifest-diff

go 1.16

require (
	github.com/Jeffail/gabs/v2 v2.6.0
	k8s.io/apimachinery v0.21.9
)
