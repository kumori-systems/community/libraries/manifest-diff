/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package diff

import (
	"reflect"
	"sort"
	"strconv"

	"github.com/Jeffail/gabs/v2"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
)

type Element struct {
	Path      []string
	Container *gabs.Container
}

// Diff calculates the differences between two JSON documents (in gabs format)
// An element will not be compared if contains a key and value indicated in the
// skip map. If the value in the skip map is "nil", the element is skipped if
// contains the key without evaluating the value.
func Diff(
	old *gabs.Container,
	new *gabs.Container,
	skip map[string]interface{},
) (changed [][]string, added [][]string, removed [][]string) {
	changed, removed = compare(old, new, skip, false)
	_, added = compare(new, old, skip, true)
	return
}

func compare(
	from *gabs.Container,
	to *gabs.Container,
	skip map[string]interface{},
	onlyMissing bool,
) (changed [][]string, missing [][]string) {
	// fmt.Printf("Json1 type: %s", reflect.TypeOf(from.Data()).String())
	pending := make([]Element, 0, 100)
	pending = append(pending, Element{
		Path:      []string{},
		Container: from,
	})

	changed = [][]string{}
	missing = [][]string{}

	var elem Element
	for len(pending) > 0 {
		elem, pending = pending[0], pending[1:]
		switch elem.Container.Data().(type) {
		case map[string]interface{}:
			themap := elem.Container.Data().(map[string]interface{})

			// Skips this element if contains the key and value (or only the
			// key if the skip value is nil).
			if skip != nil {
				skipit := false
				for skipKey, skipValue := range skip {
					if value, ok := themap[skipKey]; ok {
						if skipValue == nil {
							skipit = true
							break
						}
						if apiequality.Semantic.DeepEqual(skipValue, value) {
							skipit = true
							break
						}
					}
				}
				if skipit {
					continue
				}
			}

			// Appends the elements in this map to be compared
			keys := GetOrderedKeys(&themap)
			for _, key := range keys {
				thepath := make([]string, 0, len(elem.Path)+1)
				if len(elem.Path) > 0 {
					thepath = append(thepath, elem.Path...)
				}
				thepath = append(thepath, key)
				if ok := to.Exists(thepath...); !ok {
					missing = append(missing, thepath)
					continue
				}
				child1Elem := elem.Container.Search(key)
				// fmt.Printf("\nThepath %s: %v", key, thepath)
				pending = append(pending, Element{
					Path:      thepath,
					Container: child1Elem,
				})
			}
		case []interface{}:
			theslice := elem.Container.Data().([]interface{})
			for key := range theslice {
				thepath := make([]string, 0, len(elem.Path)+1)
				if len(elem.Path) > 0 {
					thepath = append(thepath, elem.Path...)
				}
				skey := strconv.Itoa(key)
				thepath = append(thepath, skey)
				if ok := to.Exists(thepath...); !ok {
					missing = append(missing, thepath)
					continue
				}
				child1Elem := elem.Container.Search(skey)
				// fmt.Printf("\nThepath %s: %v", skey, thepath)
				pending = append(pending, Element{
					Path:      thepath,
					Container: child1Elem,
				})
			}
		case float64:
			container2 := to.Search(elem.Path...)
			data1 := elem.Container.Data()
			data2 := container2.Data()
			if (data1 == nil) && (data2 == nil) {
				continue
			}
			if (data1 == nil) || (data2 == nil) {
				missing = append(missing, elem.Path)
				continue
			}
			if onlyMissing {
				continue
			}
			if reflect.TypeOf(data2).Name() != "float64" {
				changed = append(changed, elem.Path)
				continue
			}

			value1 := data1.(float64)
			value2 := data2.(float64)
			if value1 != value2 {
				changed = append(changed, elem.Path)
			}
		case string:
			container2 := to.Search(elem.Path...)
			data1 := elem.Container.Data()
			data2 := container2.Data()
			if (data1 == nil) && (data2 == nil) {
				continue
			}
			if (data1 == nil) || (data2 == nil) {
				missing = append(missing, elem.Path)
				continue
			}
			if onlyMissing {
				continue
			}
			if reflect.TypeOf(data2).Name() != "string" {
				changed = append(changed, elem.Path)
				continue
			}

			value1 := data1.(string)
			value2 := data2.(string)
			if value1 != value2 {
				changed = append(changed, elem.Path)
			}
		case bool:
			container2 := to.Search(elem.Path...)
			data1 := elem.Container.Data()
			data2 := container2.Data()
			if (data1 == nil) && (data2 == nil) {
				continue
			}
			if (data1 == nil) || (data2 == nil) {
				missing = append(missing, elem.Path)
				continue
			}
			if onlyMissing {
				continue
			}
			if reflect.TypeOf(data2).Name() != "bool" {
				changed = append(changed, elem.Path)
				continue
			}

			value1 := data1.(bool)
			value2 := data2.(bool)
			if value1 != value2 {
				changed = append(changed, elem.Path)
			}
		default:
		}
	}
	return changed, missing
}

// GetOrderedKeys returns an ordered array with the keys of a given map
func GetOrderedKeys(themap *map[string]interface{}) []string {
	keys := make([]string, 0, len(*themap))
	for key := range *themap {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}
