/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package diff

import (
	"strings"
	"testing"

	"github.com/Jeffail/gabs/v2"
)

func TestDiff(t *testing.T) {
	json1, err := gabs.ParseJSON([]byte(`{
		"outter":{
			"same":{
				"value1":1,
				"value2":"string",
				"value3":true
			},
			"changed":{
				"value1":1,
				"value2":"string",
				"value3":true
			},
			"missing1":{
				"value":1
			},
			"array":{
				"value1":20,
				"array1":[
					30, 40
				]
			},
			"skip":{
				"inherited": true,
				"value": 1
			}
		}
	}`))
	if err != nil {
		panic(err)
	}
	json2, err := gabs.ParseJSON([]byte(`{
		"outter":{
			"same":{
				"value1":1,
				"value2":"string",
				"value3":true
			},
			"changed":{
				"value1":2,
				"value2":"changed",
				"value3":false
			},
			"missing2":{
				"value":1
			},
			"array":{
				"value1":20,
				"array1":[
					30, 40
				]
			},
			"skip":{
				"inherited": true,
				"value": 2
			}
		}
	}`))
	if err != nil {
		panic(err)
	}

	expectedChanged := [][]string{
		{"outter", "changed", "value1"},
		{"outter", "changed", "value2"},
		{"outter", "changed", "value3"},
	}

	expectedAdded := [][]string{
		{"outter", "missing2"},
	}

	expectedRemoved := [][]string{
		{"outter", "missing1"},
	}

	skip := map[string]interface{}{
		"inherited": true,
	}

	changed, added, removed := Diff(json1, json2, skip)

	if len(changed) != len(expectedChanged) {
		t.Errorf("Changed length differ. Expected: %d. Found: %d", len(expectedChanged), len(changed))
	} else {
		for i, elem := range changed {
			for j, epath := range elem {
				if epath != expectedChanged[i][j] {
					t.Errorf("Changed. Element %d position %d differ. Expected: %s Found: %s", i, j, strings.Join(expectedChanged[i], "/"), strings.Join(elem, "/"))
				}
			}
		}
	}

	if len(added) != len(expectedAdded) {
		t.Errorf("Changed length differ. Expected: %d. Found: %d", len(expectedAdded), len(added))
	} else {
		for i, elem := range added {
			for j, epath := range elem {
				if epath != expectedAdded[i][j] {
					t.Errorf("Added. Element %d position %d differ. Expected: %s Found: %s", i, j, strings.Join(expectedAdded[i], "/"), strings.Join(elem, "/"))
				}
			}
		}
	}

	if len(removed) != len(expectedRemoved) {
		t.Errorf("Changed length differ. Expected: %d. Found: %d", len(expectedRemoved), len(removed))
	} else {
		for i, elem := range removed {
			for j, epath := range elem {
				if epath != expectedRemoved[i][j] {
					t.Errorf("Removed. Element %d position %d differ. Expected: %s Found: %s", i, j, strings.Join(expectedRemoved[i], "/"), strings.Join(elem, "/"))
				}
			}
		}
	}
}
